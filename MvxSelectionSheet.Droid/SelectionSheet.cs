﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using MvxSelectionSheet.Core;

namespace MvxSelectionSheet.Droid
{
	public class SelectionSheet : ISelectionSheet
	{
		protected Activity CurrentActivity {
			get { return Mvx.Resolve<IMvxAndroidCurrentTopActivity> ().Activity; }
		}

		public Task<TEntity> SelectSheetAsync<TEntity> (string title, string message, IList<TEntity> sheets) where TEntity : class, ISheet
		{
			var tcs = new TaskCompletionSource<TEntity> ();
			var items = sheets.Select (x => x.Name).ToArray ();
			Application.SynchronizationContext.Post (ignored => {
				if (CurrentActivity == null) return;
				new AlertDialog.Builder (CurrentActivity)					
					.SetTitle (title)
					.SetSingleChoiceItems (items, sheets.IndexOf(sheets.FirstOrDefault(x=>x.Default)), (obj, e) => {
						tcs.TrySetResult(sheets [e.Which]);
						(obj as AlertDialog).Dismiss();
					}).SetCancelable(false)
	                .SetNegativeButton("Cancel",(sender, e) => tcs.TrySetResult (null))
					.Show ();
				
			}, null);
			return tcs.Task;
		
		}
	}
}

