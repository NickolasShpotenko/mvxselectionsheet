﻿namespace MvxSelectionSheet.Core
{
	public interface ISheet
	{
		string Name { get; set; }
		bool Default { get; set; }
	}
}

