﻿namespace MvxSelectionSheet.Core
{
	using System.Collections.Generic;
	using System.Threading.Tasks;

	public interface ISelectionSheet
	{
		Task<TEntity> SelectSheetAsync<TEntity> (string title, string message, IList<TEntity> sheets) where TEntity : class, ISheet;
	}
}

