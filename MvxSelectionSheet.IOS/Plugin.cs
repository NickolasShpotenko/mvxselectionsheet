﻿namespace MvxSelectionSheet.IOS
{
	using MvvmCross.Platform;
	using MvvmCross.Platform.Plugins;
	using Core;

	public class Plugin : IMvxPlugin
	{
		public void Load ()
		{
			Mvx.RegisterSingleton<ISelectionSheet> (new SelectionSheet ());
		}
	}
}

