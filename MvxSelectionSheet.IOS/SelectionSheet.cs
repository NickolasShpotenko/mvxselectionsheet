﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using MvxSelectionSheet.Core;
using UIKit;

namespace MvxSelectionSheet.IOS
{
	public class SelectionSheet : ISelectionSheet
	{
		public Task<TEntity> SelectSheetAsync<TEntity> (string title, string message, IList<TEntity> sheets) where TEntity : class, ISheet
		{
			var task = new TaskCompletionSource<TEntity> ();
			UIAlertController alertController = UIAlertController.Create (title, message, UIAlertControllerStyle.ActionSheet);

			foreach (var item in sheets) {
				alertController.AddAction (UIAlertAction.Create (item.Name, UIAlertActionStyle.Default, (obj) => task.SetResult (item)));
			}
			alertController.AddAction (UIAlertAction.Create ("Cancel", UIAlertActionStyle.Cancel, (obj) => task.SetResult(null)));

			var controller = (Mvx.Resolve<IMvxIosViewPresenter> () as MvxIosViewPresenter).MasterNavigationController.TopViewController;
			UIPopoverPresentationController presentationPopover = alertController.PopoverPresentationController;
			if (presentationPopover != null) {
				presentationPopover.SourceView = controller.View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			controller.PresentViewController (alertController, true, null);
			return task.Task;
		}
	}
}

